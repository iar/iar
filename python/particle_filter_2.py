import numpy as np
from math import cos, sin, sqrt, pi
from types import IntType
from calibration import Ir2Cm
from util import Util
from class_profiler import *


IMAGE_SCALE = 1.

# The distance at which objects can be seen, as measured from the edge of the robot.
DISTANCE_THRESHOLD = 80

# arbitrary color value, used to separate black from white in the picture.
GRID_THRESHOLD = 50

HOME_LOCATION_PX_XY = (int(1570 * IMAGE_SCALE), int(1386 * IMAGE_SCALE))

# Data indices in the numpy array, representing a particle object.
INDEX_X = 0
INDEX_Y = 1
INDEX_TH = 2
INDEX_OFFSET_TH = 3
INDEX_WEIGHT = 4


class ParticleFilter:
    """
    Class, responsible for localising the robot by the use of particle filters.
    """
    PARTICLE_COUNT = 720
    RESAMPLED_COUNT = 0

    # ARENA_SIZE, mm
    # Note that image of size [3240, 1764] is of roughly the same ratio
    ARENA_SIZE_MM = np.array([1400, 760])

    @profile
    def __init__(self, mask_file="map_images/arena_masked.npy"):
        # Debug statistics
        self.good_replacements = 0
        self.particles_placed_in_objects = 0
        self.profiler = dict()

        # Make map of given scale.
        from map_images.mask_maker import MaskMaker

        MaskMaker.make_array("map_images/")

        # Load map
        self.map = np.load(mask_file)
        self.map_shape = self.map.shape

        # Declare constants
        self.PX_PER_MM = self.map_shape[1] / 1400.
        self.MM_PER_PX = 1400. / self.map_shape[1]

        # Sensors-related variables, for better map object position estimation
        self.KHEPERA_RADIUS_MM = 27.5
        self.KHEPERA_RADIUS_PX = self.KHEPERA_RADIUS_MM * self.PX_PER_MM

        # The cap at which an object can be seen, from the robot's center.
        self.MAX_DISTANCE = int((DISTANCE_THRESHOLD + self.KHEPERA_RADIUS_MM) * self.PX_PER_MM)
        assert type(self.MAX_DISTANCE) is IntType

        # Initialize particles
        self.particles = self._normal_generate_particles(self.PARTICLE_COUNT,
                                                         (HOME_LOCATION_PX_XY[0], HOME_LOCATION_PX_XY[1], 0.),
                                                         1,  # self.KHEPERA_RADIUS_PX * 0.75,
                                                         1. / self.PARTICLE_COUNT,
                                                         angle_gen="uniform")

    @profile
    def _normal_generate_particles(self, num_particles, pose, std, weight, angle_gen="random"):
        """
        Generates and returns a number of particles, based on a normal distribution.
        The resulting angles are completely random.

        :param num_particles:   Number of particles to generate.
        :param pose:            (x, y, th) of the centerpoint, in px.
        :param std:             Standard deviation of the normal distribution.
        :param weight:          The weight of every generated particle.
        :param angle_gen:       ["random", "uniform"], corresponding to the method of angle generation.
        :return np.array([[x, y, th, offset_th, weight], ...])
        """
        xs = np.random.normal(pose[0], scale=std, size=num_particles)
        ys = np.random.normal(pose[1], scale=std, size=num_particles)
        if angle_gen.lower() == "random":
            thetas = np.random.rand((num_particles)) * np.pi * 2.  # Don't remove the extra parentheses!
        elif angle_gen.lower() == "uniform":
            thetas = np.random.uniform(0, 2. * np.pi, size=num_particles)
        weights = np.ones(num_particles) * weight
        return np.array([xs, ys, thetas, thetas, weights]).T

    @profile
    def raycast(self, x, y, th, algorithm="picky"):
        """
        Give the distance to closest object when given robot location and sensor's orientation.
        It is assumed that sensors face directly away from khepera's center - DOES NOT take int account that sensors
        could be facing forward, but be at a slight angle.

        :param x:           khepera's x, in pixels
        :param y:           khepera's y, in pixels, reversed y
        :param th:          sensor's orientation, reversed y in radians
        :param algorithm:   Choose which algorithm to use from ["picky", "forceful"].
                            "Picky" is simpler, but can might overshoot. Also, it is resolution-independent.
                            "Forceful" offers better speeds as it's vectorised and uses numpy objects.
                            However, due to its brute-force nature, it is heavily slowed down by resolution.

        :return: distance to the closest point found, in pixels. If no points are found withing self.MAX_DISTANCE,
                 self.MAX_DISTANCE will be returned
        """
        if algorithm.lower() == "picky":
            distances = np.array(Ir2Cm.distances) * self.PX_PER_MM * 10

            xy = np.array([x, y]) + np.array([self.KHEPERA_RADIUS_PX] * 2)
            for i, dist in enumerate(distances[1:]):
                delta = Util.pol2cart(dist, th)
                projection = xy + delta
                if self._outside_map(projection) or self._is_obstacle(projection):
                    return sum(distances[i - 1:i + 1]) / 2
            return distances[-1]

        elif algorithm.lower() == "forceful":
            # Find all indices to be scanned:
            # 1) Create array [[1,1], [2,2], ..., [max_dist-1, max_dist-1]]
            # 2) Add the current sensor position and then rotate everything around by theta. Cast to integer at the end.
            coords = np.array((np.arange(self.MAX_DISTANCE), np.arange(self.MAX_DISTANCE))).transpose()
            coords = ([y, x] + (coords + self.KHEPERA_RADIUS_MM) * [sin(th), cos(th)]).astype(int)

            # Remove the resulting coordinates out of the grid
            # I assume duplicates won't cause much of an issue.
            coords = coords[coords[:, 0] >= 0]
            coords = coords[coords[:, 0] < self.map_shape[0]]
            coords = coords[coords[:, 1] >= 0]
            coords = coords[coords[:, 1] < self.map_shape[1]]

            # Return coordinates of occupied cells.
            occupied = np.array([ind for ind in coords if self.map[ind[0]][ind[1]] <= GRID_THRESHOLD])

            # Get the squared distance to occupied cells
            if occupied.size != 0:
                dists_sqrd = np.sum((occupied - [y, x]) ** 2, axis=1)
                return sqrt(min(dists_sqrd))
            else:
                return 1. * self.MAX_DISTANCE

    @profile
    def _outside_map(self, point):
        x = round(point[0])
        y = round(point[1])
        return x < 0 or y < 0 or y >= self.map_shape[0] or x >= self.map_shape[1]

    @profile
    def _is_obstacle(self, point):
        return self.map[round(point[1])][round(point[0])] <= GRID_THRESHOLD

    @profile
    def step(self, delta_pose, sensor_values, mode="wave"):
        """
        Update particle positions and resample, based on probabilities. Update debugging information.

        :param delta_pose:     (x, y, orientation(in radians)). Changes since last time step. Normal y direction.
        :param sensor_values:   IR sensors values, in millimeters.
        :param mode:            mode of operation from ["wave", "normal"].
                                Wave mode does no track weights and only removes particles when they hit objects.
                                Normal mode performs normal particle filters.
        """
        if mode.lower() == "wave":
            # Coordinate system conversions
            delta_pose = list(delta_pose)
            delta_pose[0] *= self.PX_PER_MM
            delta_pose[1] = -delta_pose[1] * self.PX_PER_MM
            delta_pose[2] = -delta_pose[2]

            if len(self.particles) > 0:
                # Perform one step
                self._update(delta_pose)
                self.particles = self._remove_invalid_from_set(self.particles)

        elif mode.lower() == "normal":
            # Coordinate system conversions
            delta_pose = list(delta_pose)
            delta_pose[0] *= self.PX_PER_MM
            delta_pose[1] = -delta_pose[1] * self.PX_PER_MM
            delta_pose[2] = -delta_pose[2]

            if len(self.particles) > 0:
                # Perform one step
                self._update(delta_pose)
                self.particles = self._remove_invalid_from_set(self.particles)
                sensor_values = np.array(sensor_values) * self.PX_PER_MM * 10
                self._calc_weight(sensor_values)

                # Remove at most the RESAMPLED_COUNT particles in total. But never less than 0.
                remove = max(0, self.RESAMPLED_COUNT - (self.PARTICLE_COUNT - len(self.particles)))
                self._remove_lowest(remove)

            add = self.PARTICLE_COUNT - len(self.particles)
            if add > 0:
                self.particles = np.concatenate((self.particles, self._make_new_likely(add)))

    @profile
    def _update(self, delta_pose):
        """
        Update particle positions.
        Perform conversion to reversed y, pixel coordinate space.

        :param delta_pose:  (x, y, orientation(in radians)), in pixel space.
        """
        dx, dy, dtheta = delta_pose
        self.particles[:, INDEX_X] += \
            dx * np.cos(self.particles[:, INDEX_OFFSET_TH]) - dy * np.sin(self.particles[:, INDEX_OFFSET_TH])
        self.particles[:, INDEX_Y] += \
            dx * np.sin(self.particles[:, INDEX_OFFSET_TH]) + dy * np.cos(self.particles[:, INDEX_OFFSET_TH])
        self.particles[:, INDEX_TH] = (self.particles[:, INDEX_TH] + dtheta) % (2. * pi)

    @profile
    def _remove_invalid_from_set(self, particles):
        """
        Removes invalid particles from a given particle set.

        :param particles:   Particle set
        :return:            Clean set.
        """
        particles = np.array(
            [p for p in particles if
             not self._outside_map((p[INDEX_X], p[INDEX_Y])) and not self._is_obstacle((p[INDEX_X], p[INDEX_Y]))
             ]
        )
        return particles

    @profile
    def _calc_weight(self, sensor_values):
        """
        Calculate particle weights based on estimated and actual sensor reading.
        Sort the particles in weight-increasing order.

        :param sensor_values:    IR sensor value in px. [left, front, right, back]
        """
        maxp = -1
        minp = float("inf")
        for p in self.particles:
            similarity = 0
            for i, angle in enumerate([-pi / 2, 0, pi / 2, pi]):
                estimate = self.raycast(
                    p[INDEX_X], p[INDEX_Y], (p[INDEX_TH] + angle) % (pi / 2),
                    algorithm="picky"
                )

                similarity += 1 / (1 + np.linalg.norm(sensor_values[i] - estimate))
            p[INDEX_WEIGHT] = similarity

        self.particles = self.particles[self.particles[:, INDEX_WEIGHT].argsort()]

    @profile
    def _remove_lowest(self, n):
        """
        Removes the given number of particles with lowest weights.

        :param n:   number of particles to remove.
        """
        n = max(n, 0)
        self.particles = self.particles[n:]

    @profile
    def _get_likeliest(self):
        """
        Gets the particle with the largest weight
        :return: np.array([x, y, th, offset_th, weight])
        """
        return self.particles[-1]

    @profile
    def _make_new_likely(self, n):
        """
        Returns a new array of n valid particles, similar to the most likely particle
        """
        x, y, th, offset_th, weight = self._get_likeliest()
        new_arr = self._normal_generate_particles(n, (x, y, th + offset_th), self.KHEPERA_RADIUS_PX * 0.5, weight)

        # Ensure no invalid entries are added.
        prev_arr = np.copy(new_arr)
        new_arr = self._remove_invalid_from_set(new_arr)
        diff = len(prev_arr) - len(new_arr)

        while diff != 0:
            new_arr = self._make_new_likely(diff)
            prev_arr = np.copy(new_arr)
            new_arr = self._remove_invalid_from_set(new_arr)
            diff = len(prev_arr) - len(new_arr)

        return new_arr

    @profile
    def get_pose(self, convert_back=True):
        """
        Returns the expected pose of the robot.
        By default, converts to unreversed y, millimeter coordinate space.
        :param convert_back:    boolean - false if to keep in reversed y, pixel space.
        :return:    (x, y, orient)
        """

        x, y, th, offset_th, _ = self._get_likeliest()
        th += offset_th

        if convert_back:
            x *= self.MM_PER_PX
            y = (self.map_shape[0] - y) * self.MM_PER_PX
            th = (-th) % (2. * pi)

        return x, y, th
