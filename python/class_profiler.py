from time import time

def profile(func):
    """
    TO USE: add 'self.profiler = dict()' to the constructor of the class you are profiling.
    Decorator that tracks function runtimes in a dictionary for debugging purposes.
    """

    def func_wrapper(self, *args, **kwargs):
        t0 = time()
        res = func(self, *args, **kwargs)
        dt = time() - t0

        function_name = func.__name__

        if function_name not in self.profiler:
            self.profiler[function_name] = (dt, dt, 1.)
        else:
            z = self.profiler[function_name]
            self.profiler[function_name] = (z[0] + dt, (z[1] * z[2] + dt) / (z[2] + 1), (z[2] + 1))

        return res

    return func_wrapper


def profile_other(func, self, *args, **kwargs):
    """
    Method that profiles a function, not belonging to class
    """
    t0 = time()
    res = func(*args, **kwargs)
    dt = time() - t0

    function_name = func.__name__

    if function_name not in self.profiler:
        self.profiler[function_name] = (dt, dt, 1.)
    else:
        z = self.profiler[function_name]
        self.profiler[function_name] = (z[0] + dt, (z[1] * z[2] + dt) / (z[2] + 1), (z[2] + 1))

    return res


def print_profiler(obj):
    """
    Pretty prints the logs, stored in the profiler.
    """
    for key in obj.profiler:
        print "Function %30s: total %10f, average %10f, run count %10d." % (
            key, obj.profiler[key][0], obj.profiler[key][1], obj.profiler[key][2]
        )
