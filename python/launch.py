from __future__ import print_function

import sys
from threading import Thread
import traceback

from command_logger import CommandLogger
from control import Connection, MockConnection
from plotting import Plotter, MockPlotter
from particle_filter_2 import ParticleFilter
from robot import Robot

class Launcher(object):
    def __init__(self):
        self.closed = False
        self.command_logger = CommandLogger()
        self.conn = None
        self.plotter = None
        self.robot = None
        self.robot_thread = Thread(target=self.robot_task, name="Robot Thread")
        # self.particle_filter = ParticleFilter()
        self.plotting_thread = Thread(
            target=self.plotting_task, name="Plotting Thread")

    def launch(self, mock):
        print("[LAUNCH] launched")

        if mock:
            self.conn = MockConnection(self.command_logger)
        else:
            self.conn = Connection(self.command_logger)
        self.conn.connect()
        try:
            self.conn.verbose = False
            self.conn.move_enabled = True
            if not self.conn.test_connection():
                print("[ERR] not connected")
            else:
                print("[CONN] connected")

            if mock:
                pass
                # self.plotter = MockPlotter()
            else:
                pass
                # self.plotter = Plotter(particle_filter=self.particle_filter)
            # self.plotting_thread.start()

            self.robot = Robot(self.conn, -1, -1)
            self.robot_thread.start()

            self.monitor_task()
        except KeyboardInterrupt:
            print("[LAUNCH] KeyboardInterrupt")
        except:
            traceback.print_exc()
        finally:
            self.close()

    def monitor_task(self):
        print("[MONITOR] launched")
        command = None
        while command != "q" and not self.closed:
            command = raw_input("")
            self.robot.set_task(command)
        print("[MONITOR] closed")

    def robot_task(self):
        print("[ROBOT] launched")
        self.robot.launch()
        print("[ROBOT] closed")

    def plotting_task(self):
        print("[PLOT] launched")
        # self.plotter.launch()
        print("[PLOT] closed")

    def close(self):
        if not self.closed:
            print("[LAUNCH] closing...")
            # close monitor thread
            self.closed = True

            # close robot thread
            if self.robot is not None:
                self.robot.close()
                self.robot_thread.join()

            # close plotting thread
            # if self.plotter is not None:
            #     self.plotter.close()
            #     self.plotting_thread.join()

            # stop and disconnect robot
            print("[LAUNCH] sending stop command")
            self.conn.stop()
            print("[LAUNCH] stop command sent")
            self.conn.disconnect()
            print("[LAUNCH] closed")


def main(mock):
    launcher = Launcher()
    launcher.launch(mock)

if __name__ == "__main__":
    try:
        mock = sys.argv[1] == "-m"
    except IndexError:
        mock = False
    main(mock)
