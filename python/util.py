import math
import numpy as np

class Util:
    def __init__(self):
        pass

    @staticmethod
    def get_direction(dept, dest):
        """
        angles: right = 0 (origin), down = -90, top = 90, left = 180
        range: -180 < angle <= 180
        """

        # y axis is reflected
        x = dest[0] - dept[0]
        y = dest[1] - dept[1]
        # tan(angle) = opposite / adjacent
        # using atan2 look at the api for it in math
        distance = math.sqrt(x * x + y * y)
        angle = math.degrees(math.atan2(y, x))

        return {'distance': distance, 'angle': angle}

    @staticmethod
    def get_turn_angle(facing, angle):
        # angles: right = 0 (origin), down = -90, top = 90, left = 180
        # range: -180 < angle <= 180
        turn_angle = (facing - angle) % 360
        if turn_angle > 180:
            turn_angle -= 360
        return -turn_angle

    @staticmethod
    def get_smallest_angle(angle1, angle2):
        return abs(Util.get_turn_angle(angle1, angle2))

    @staticmethod
    def pol2cart(rho, phi):
        dx = rho * np.cos(phi)
        dy = rho * np.sin(phi)
        return np.array([dx, dy])
