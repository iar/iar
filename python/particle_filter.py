import numpy as np
from scipy.stats import norm
from random import random
from math import cos, sin, sqrt, pi, exp
from types import IntType
from calibration import Ir2Cm
from util import Util

from map_images.mask_maker import GRID_THRESHOLD

SENSOR_ERROR_VARIANCE = 0.9 ** 2

# The distance at which objects can be seen, as measured from the edge of the robot.
DISTANCE_THRESHOLD = 80


class ParticleFilter:
    """
    Class, responsible for localising the robot by the use of particle filters.
    """
    PARTICLE_COUNT = 30
    RESAMPLED_COUNT = 50

    # ARENA_SIZE, mm
    # Note that image of size [3240, 1764] is of roughly the same ratio
    ARENA_SIZE_MM = np.array([1400, 760])
    PX_PER_MM = 3240. / 1400.
    MM_PER_PX = 1400. / 3240.

    # Sensors-related variables, for better map object position estimation
    KHEPERA_RADIUS_MM = 27.5
    KHEPERA_RADIUS_PX = KHEPERA_RADIUS_MM * PX_PER_MM

    # The cap at which an object can be seen, from the robot's center.
    MAX_DISTANCE = int((DISTANCE_THRESHOLD + KHEPERA_RADIUS_MM) * PX_PER_MM)
    assert type(MAX_DISTANCE) is IntType

    def __init__(self, mask_file="map_images/arena_masked.npy"):
        # Debug statistics
        self.good_replacements = 0
        self.particles_placed_in_objects = 0

        self.map = np.load(mask_file)
        self.map_shape = self.map.shape
        self.particles = [self.Particle(self) for _ in xrange(self.PARTICLE_COUNT)]

    # def raycast(self, x, y, orient):
    #     """
    #     Give the distance to closest object when given robot location and sensor's orientation.
    #     It is assumed that sensors face directly away from khepera's center - DOES NOT take int account that sensors
    #     could be facing forward, but be at a slight angle.
    #
    #     :param x:       khepera's x, in pixels
    #     :param y:       khepera's y, in pixels, reversed y
    #     :param orient:  sensor's orientation, reversed y in radians
    #     :return: distance to the closest point found, in pixels. If no points are found withing MAX_DISTANCE,
    #              MAX_DISTANCE will be returned
    #     """
    #     # Find all indices to be scanned:
    #     # 1) Create array [[1,1], [2,2], ..., [max_dist-1, max_dist-1]]
    #     # 2) Add the current sensor position and then rotate everything around by theta. Cast to integer at the end.
    #     coords = np.array((np.arange(self.MAX_DISTANCE), np.arange(self.MAX_DISTANCE))).transpose()
    #     coords = ([y, x] + (coords + self.KHEPERA_RADIUS_MM) * [sin(orient), cos(orient)]).astype(int)
    #
    #     # Remove the resulting coordinates out of the grid
    #     # I assume duplicates won't cause much of an issue.
    #     coords = coords[coords[:, 0] >= 0]
    #     coords = coords[coords[:, 0] < self.map_shape[0]]
    #     coords = coords[coords[:, 1] >= 0]
    #     coords = coords[coords[:, 1] < self.map_shape[1]]
    #
    #     # Return coordinates of occupied cells.
    #     occupied = np.array([ind for ind in coords if self.map[ind[0]][ind[1]] <= GRID_THRESHOLD])
    #
    #     # Get the squared distance to occupied cells
    #     if occupied.size != 0:
    #         dists_sqrd = np.sum((occupied - [y, x]) ** 2, axis=1)
    #         return sqrt(min(dists_sqrd))
    #     else:
    #         return 1. * self.MAX_DISTANCE

    def raycast(self, x, y, th):
        """
        Give the distance to closest object when given robot location and sensor's orientation.
        It is assumed that sensors face directly away from khepera's center - DOES NOT take int account that sensors
        could be facing forward, but be at a slight angle.

        :param x:       khepera's x, in pixels
        :param y:       khepera's y, in pixels, reversed y
        :param th:  sensor's orientation, reversed y in radians
        :return: distance to the closest point found, in pixels. If no points are found withing MAX_DISTANCE,
                 MAX_DISTANCE will be returned
        """
        distances = np.array(Ir2Cm.distances) * self.PX_PER_MM * 10

        xy = np.array([x, y]) + np.array([self.KHEPERA_RADIUS_PX] * 2)
        for i, dist in enumerate(distances[1:]):
            delta = Util.pol2cart(dist, th)
            projection = xy + delta
            if self.outside_map(projection) or self.is_obstacle(projection):
                return sum(distances[i-1:i+1])/2
        return distances[-1]

    def outside_map(self, point):
        x = round(point[0])
        y = round(point[1])
        return x < 0 or y < 0 \
            or y >= self.map_shape[0] or x >= self.map_shape[1]

    def is_obstacle(self, point):
        return self.map[round(point[1])][round(point[0])] <= GRID_THRESHOLD

    def resample(self):
        """
        Select which particles to resample, based on their probability, and replace the negligible ones..

        :return:   Set of indices of kept particles.
        """
        total_probability = sum([p.prob for p in self.particles])
        particles_kept = set()
        for n in xrange(self.RESAMPLED_COUNT):
            self.good_replacements += 1

            stop = random() * total_probability
            cumulative_prob = 0
            particle_index = 0
            while cumulative_prob < stop:
                cumulative_prob += self.particles[particle_index].prob
                particle_index += 1
            particles_kept.add(self.particles[particle_index - 1])
        return particles_kept

    def update(self, delta_pose, sensor_values):
        """
        Update particle positions and resample, based on probabilities. Update debugging information.
        Perform conversion to reversed y, pixel coordinate space.

        :param delta_pose:     (x, y, orientation(in radians)). Changes since last time step. Normal y direction.
        :param sensor_values:   IR sensors values, in millimeters.
        """
        # Coordinate system conversions
        delta_pose = list(delta_pose)
        delta_pose[0] *= ParticleFilter.PX_PER_MM
        delta_pose[1] = -delta_pose[1] * ParticleFilter.PX_PER_MM
        delta_pose[2] = -delta_pose[2]

        for p in self.particles:
            p.update_particle(delta_pose)
            p.calc_prob(sensor_values)

            if not p.valid():
                p.replace()

        # kept = self.resample()
        # for p in set(self.particles) - kept:
        #     p.replace()

    def get_pose(self, convert_back=True):
        """
        Returns the expected pose of the robot.
        By default, converts to unreversed y, millimeter coordinate space.
        :param convert_back:    boolean - false if to keep in reversed y, pixel space.
        :return:    (x, y, orient)
        """
        total_probability = sum([p.prob for p in self.particles])
        total_x = total_y = total_th = 0
        for p in self.particles:
            total_x += p.prob / total_probability * p.x
            total_y += p.prob / total_probability * p.y
            total_th += (p.prob / total_probability * (p.th)) % (2. * pi)
        # TODO test with simple average rather than weighted average

        if convert_back:
            total_x *= self.MM_PER_PX
            total_y = (self.map_shape[0] - total_y) * self.MM_PER_PX
            total_th = (-total_th) % (2. * pi)

        return total_x, total_y, total_th

    class Particle:
        def __init__(self, particle_filter):
            self.prob = 1.
            self.particle_filter = particle_filter
            self.x = None
            self.y = None
            self.th = 0
            self.offset_th = None  # Initial orientation, corresponding to axes projected on.
            self.replace()

        def replace(self):
            """
            Places the particle in a new location
            """
            self.x = self.particle_filter.KHEPERA_RADIUS_MM + \
                     random() * (self.particle_filter.map.shape[1] - 2 * self.particle_filter.KHEPERA_RADIUS_MM)
            self.y = self.particle_filter.KHEPERA_RADIUS_MM + \
                     random() * (self.particle_filter.map.shape[0] - 2 * self.particle_filter.KHEPERA_RADIUS_MM)

            self.offset_th = random() * 2. * pi
            self.th = self.offset_th

            if not self.valid():
                self.particle_filter.particles_placed_in_objects += 1  # DEBUG data
                self.replace()

        def valid(self):
            """
            :return:    True if map is unoccupied at particle location.
                        False otherwise.
            """
            def point_valid(point):
                return not self.particle_filter.outside_map(point) and \
                    not self.particle_filter.is_obstacle(point)
            point = np.array([self.x, self.y])
            points = [point + Util.pol2cart(self.particle_filter.KHEPERA_RADIUS_PX, th)
                      for th in [0, pi*0.5, pi*-0.5, pi]] + [point]
            return all(point_valid(point) for point in points)

        def update_particle(self, delta_pose):
            """
            Updates pose of a particle.
            :param delta_pose: (x, y, theta) changes since last time step, in pixel space.
            """
            dx, dy, dtheta = delta_pose
            self.x += dx * cos(self.offset_th)
            self.y += dx * sin(self.offset_th)
            self.x += dy * (-sin(self.offset_th))
            self.y += dy * cos(self.offset_th)
            self.th = (self.th + dtheta) % (2. * pi)

        def calc_prob(self, sensor_values):
            """
            Calculates correlation between given sensor values and expresses that as probability of particle.
            # Assuming sensors values of [left, left_side, front_1, front_2, right_side, right, back_right, back_left]
            Assuming sensor values of [left, front, right, back]

            :param sensor_values:   IR sensor values.
            """
            if not self.valid():
                self.prob = 0.0
            else:
                sim = 0
                for i, angle in enumerate([-pi/2, 0, pi/2, pi]):
                    est = self.particle_filter.raycast(
                        self.x, self.y,
                        (self.th + angle) % (pi / 2)
                    ) * (self.particle_filter.MM_PER_PX / 10)
                    diff = 1 / (1 + np.linalg.norm(sensor_values[i] - est))
                    sim += diff
                self.prob = sim / 4

