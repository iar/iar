from __future__ import print_function

from threading import Lock
from pylab import *
import matplotlib.image as mpimg
from util import Util
from particle_filter_2 import IMAGE_SCALE
from class_profiler import *


class Plotter(object):
    def __init__(self, particle_filter=None):
        self.closed = False
        self.profiler = dict()

        self.khepera_radius = 27.5  # mm

        self.odometry_lock = Lock()
        self.xs = []
        self.ys = []
        self.ths = []
        self.sample_time = 0
        self.path = None
        self.khepera_circle = None
        self.angle = None
        self.sample_time_text = None

        self.obstacles_lock = Lock()
        self.obstacles_set = set([])
        self.obstacles_scatter = None

        self.warn_particles_disappearing = True

        if particle_filter:
            self.pf = particle_filter
            self.replot_pf = False
            self.map_img = mpimg.imread("map_images/arena_masked_resized.jpg")
            self.particle_objects = []
            self.particle_angles = [None] * self.pf.PARTICLE_COUNT

    def close(self):
        self.closed = True

    def launch(self):
        plt.ion()

        plt.figure(0)
        plt.grid(b=None, which='both', axis='both')
        plt.xlabel("x (mm)")
        plt.ylabel("y (mm)")
        plt.title("Path taken and current configuration")

        self.path, = plt.plot([0], [0], color='r')
        self.angle, = plt.plot([0], [0], color='g')
        self.sample_time_text = text(0, 0, "0ms")

        plt.figure(1)
        plt.imshow(self.map_img, cmap="gray")
        for x in range(self.pf.PARTICLE_COUNT):
            self.particle_angles[x] = plt.plot([0], [0], color='g')

        while not self.closed:
            plt.figure(0)
            with self.obstacles_lock:
                self.plot_obstacles()
            with self.odometry_lock:
                if len(self.xs) >= 2:
                    self.plot_path()
                    self.plot_khepera_circle()
                    self.plot_angle()
                    self.display_sample_time()

            if self.replot_pf:
                self.replot_pf = False
                plt.figure(1)
                self.plot_particles()

            plt.pause(0.05)  # render

    def plot_path(self):
        self.path.set_data(self.xs, self.ys)
        min_val = min(min(self.xs), min(self.ys)) - self.khepera_radius
        max_val = max(max(self.xs), max(self.ys)) + self.khepera_radius
        self.path.axes.axis([min_val, max_val, min_val, max_val])

    def plot_khepera_circle(self):
        if self.khepera_circle is not None:
            self.khepera_circle.remove()
        self.khepera_circle = plt.Circle(
            (self.xs[-1], self.ys[-1]), self.khepera_radius, color='b')
        plt.gcf().gca().add_artist(self.khepera_circle)

    def plot_angle(self):
        current_x, current_y = self.xs[-1], self.ys[-1]
        projection = Util.pol2cart(self.pf.KHEPERA_RADIUS_PX, self.ths[-1])
        projection_x, projection_y = projection[0], projection[1]
        self.angle.set_data([current_x, current_x + projection_x],
                            [current_y, current_y + projection_y])

    def display_sample_time(self):
        self.sample_time_text.remove()
        self.sample_time_text = text(
            self.xs[-1], self.ys[-1], str(round(self.sample_time * 1000, 1)) + "ms")

    def plot_obstacles(self):
        if self.obstacles_scatter is not None:
            try:
                self.obstacles_scatter.remove()
            except ValueError:
                print("[ERR] error removing scatter plot")
        obstacles_list = list(self.obstacles_set)
        if len(obstacles_list) >= 1:
            xsys = [list(t) for t in zip(*obstacles_list)]
            xs = xsys[0]
            ys = xsys[1]
            self.obstacles_scatter = scatter(
                xs, ys, c=np.arange(len(obstacles_list)), zorder=2)

    def add_configuration(self, x, y, th, sample_time):
        with self.odometry_lock:
            self.xs.append(x)
            self.ys.append(y)
            self.ths.append(th)
            self.sample_time = sample_time

    def update_obstacles(self, add_list, delete_list):
        with self.obstacles_lock:
            for obstacle in add_list:
                self.obstacles_set.add(obstacle)
            for obstacle in delete_list:
                self.obstacles_set.discard(obstacle)

    def clear(self):
        with self.odometry_lock:
            self.xs = []
            self.ys = []
            self.ths = []

    def update_particles(self):
        self.replot_pf = True

    def cleanup_particles(self):
        """
        Removes all objects that were plotted in the previous iteration.
        """
        while len(self.particle_objects) > 0:
            obj = self.particle_objects.pop()
            obj.remove()

    @profile
    def plot_particles(self):
        # side_coords = []
        # for i, p in enumerate(self.pf.particles):
        #     poses[i, :] = np.array([p.x, p.y, p.th, p.prob])
        #     side_c = []
        #     for side in [-.5 * pi, 0, .5 * pi, pi]:
        #         sensor_orient = (p.th + side) % (2. * pi)
        #         cast_dist = self.pf.raycast(p.x, p.y, sensor_orient)
        #         dx = p.x + cast_dist * cos(sensor_orient)
        #         dy = p.y + cast_dist * sin(sensor_orient)
        #
        #         side_c.append((dx, dy))
        #     side_coords.append(side_c)

        # Remove previous plots
        self.cleanup_particles()

        # side_coords = np.array(side_coords)
        scatter_size = 100. * 2 ** IMAGE_SCALE
        # scatter_size = self.pf.KHEPERA_RADIUS_PX*2
        if len(self.pf.particles) > 0:
            self.particle_objects.append(
                plt.scatter(x=self.pf.particles[:, 0], y=self.pf.particles[:, 1],
                            c=self.pf.particles[:, 4],
                            cmap="Blues", s=scatter_size))
        elif self.warn_particles_disappearing:
            self.warn_particles_disappearing = False
            print("[FILTER] All particles are gon :/")

        for i, pa in enumerate(self.particle_angles):
            try:
                current_x, current_y = self.pf.particles[i, 0], self.pf.particles[i, 1]
                projection = Util.pol2cart(self.pf.KHEPERA_RADIUS_PX, self.pf.particles[i, 2])
                projection_x, projection_y = projection[0], projection[1]
                pa[0].set_data([current_x, current_x + projection_x],
                               [current_y, current_y + projection_y])
            except:
                pa[0].set_data([0, 0], [0, 0])

        # print_profiler(self.pf)

        # self.particle_objects.append(
        #     plt.scatter(x=side_coords[:, :, 0].transpose(), y=side_coords[:, :, 1].transpose(), c='r', s=10))

        # # Connect the dots
        # for c in xrange(len(side_coords[0])):
        #     self.particle_objects += list(plt.plot(
        #         [poses[:, 0], side_coords[:, c, 0]],
        #         [poses[:, 1], side_coords[:, c, 1]],
        #         c='r'
        #     ))

        # Plot the estimated location
        # likely_pose = self.pf.get_pose(convert_back=False)
        # self.particle_objects.append(plt.scatter(x=likely_pose[0], y=likely_pose[1], c='b', s=40))
        # self.particle_objects += list(plt.plot(
        #     [likely_pose[0], likely_pose[0] + 100. * cos(likely_pose[2])],
        #     [likely_pose[1], likely_pose[1] + 100. * sin(likely_pose[2])],
        #     c='b'
        # ))

        # print("Plotted particles in %f" % (time() - t0))


class MockPlotter(object):
    def __init__(self):
        self.closed = False

    def close(self):
        self.closed = True

    def launch(self):
        while not self.closed:
            pass

    def add_configuration(self, x, y, th, sample_time):
        pass

    def update_obstacles(self, add_list, delete_list):
        pass

    def clear(self):
        pass
