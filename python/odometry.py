#!/usr/bin/env python

from __future__ import print_function

import numpy as np
from math import pi


def pi2pi(angle):
    angle %= 2*pi
    if angle > pi:
        angle -= 2*pi
    return angle


# all distance units in mm
# all angle units in radians (code) and degrees (display)
# all time units in s
class Odometry(object):
    def __init__(self, plotter, now, encoder_counts):
        # self.plotter = plotter
        self.distance_per_pulse = 1/12.0

        # initial configuration
        self.x, self.y, self.th = 0, 0, 0
        # self.plotter.add_configuration(self.x, self.y, self.th, 0)

        # initial wheel angles and time
        self.encoder_counts = np.array(encoder_counts)
        self.time = now

        # odometry parameters
        b = 54.7  # distance between wheels
        self.c = np.array([[0.5, 0.5], [1/b, -1/b]])

    def update(self, time, encoder_counts):
        sample_time = time - self.time
        # print('sample_time = ' + str(sample_time) + 's')

        encoder_counts = np.array(encoder_counts)

        # wheel speeds in mm/s
        vr = self.distance_per_pulse \
            * (encoder_counts[1] - self.encoder_counts[1]) / sample_time
        vl = self.distance_per_pulse \
            * (encoder_counts[0] - self.encoder_counts[0]) / sample_time
        # print('[vr, vl] = ' + str(np.array([vr, vl])) + 'mm/s')

        vw = np.dot(self.c, np.array([vr, vl]))
        v = vw[0]  # velocity in mm/s
        w = vw[1]  # angular velocity in radians/s
        # print('v = ' + str(v) + 'mm/s, w = ' + str(w * 180/pi) + 'degrees/s')

        self.x += sample_time * v * np.cos(self.th + sample_time * w / 2)
        self.y += sample_time * v * np.sin(self.th + sample_time * w / 2)
        self.th += sample_time * w
        # print('x: ' + str(self.x) + ' ' +
        #       'y: ' + str(self.y) + ' ' +
        #       'th: ' + str(pi2pi(self.th) * 180/pi))

        # self.plotter.add_configuration(self.x, self.y, self.th, sample_time)

        self.time = time
        self.encoder_counts = encoder_counts

    def clear(self):
        self.x, self.y, self.th = 0, 0, 0
        # self.plotter.clear()
