#!/usr/bin/env python

import random
from time import time, sleep

from calibration import Ir2Cm, Calibration, Sensor
from odometry import Odometry
from util import Util

from math import degrees, pi
from class_profiler import *


class Direction(object):
    NONE = 0
    LEFT = 1
    RIGHT = -1

    @staticmethod
    def decode(direction):
        if direction == 0:
            return "none"
        elif direction == 1:
            return "left"
        elif direction == -1:
            return "right"

    @staticmethod
    def random():
        return random.choice([Direction.LEFT, Direction.RIGHT])


class FoodLocations(object):
    def __init__(self):
        self.locations = []
        self.full = []

    def closest(self, current_location, home=None):
        if home is None:
            closest_distance = float("inf")
            closest_food = None
        else:
            closest_distance = Util.get_direction(
                    current_location,
                    home
                )['distance']
            closest_food = home

        for i in range(len(self.locations)):
            if self.full[i]:
                d = Util.get_direction(
                    current_location,
                    self.locations[i]
                )['distance']
                if d < closest_distance:
                    closest_distance = d
                    closest_food = self.locations[i]
        return closest_food

    def add(self, location):
        self.locations.append(location)
        self.full.append(True)

    def eat(self, location):
        for i in range(len(self.locations)):
            if location == self.locations[i]:
                self.full[i] = False

    def replenish(self):
        for i in range(len(self.locations)):
            self.full[i] = True

    def any(self):
        return self.locations != []


class Robot(object):
    def __init__(self, conn, plotter, particle_filter):
        self.conn = conn
        # self.plotter = plotter
        self.profiler = {}

        # speeds
        self.speed_turn = 3.0
        self.speed_forward = 5.0

        # top-level control
        self.closed = False
        self.task = " "
        self.task_change = False
        self.algorithm = ""
        self.subtask = ""

        # calibration
        self.ir2cm = Ir2Cm()
        self.calibration = Calibration(self.conn, iterations=5)
        self.calibration_x = 0
        self.calibration_point = None
        self.calibration_bearing = None

        # subsumption
        self.print_str = None
        self.obstacle_threshold = 5
        self.turn_direction = Direction.NONE
        self.wall_follow_distance = 2.5
        self.wall_start_follow_distance = 5
        self.correction_factor = 1.5

        # odometry, ir distances and localisation
        self.conn.set_counts(0, 0)
        self.odometry = Odometry(-1, time(), self.conn.read_counts())
        self.ir_distances = dict()
        # self.particle_filter = particle_filter
        self.localisation = dict()
        self.do_localisation = True

        # untangling
        self.untangling = False

        # go in direction
        self.distance_threshold = 5  # mm

        # task 3
        self.timers = {}
        self.desired_location = None
        self.home = (0, 0)
        self.food_locations = FoodLocations()
        self.has_food = False
        self.conn.set_led(0, 0)

        # bug0
        self.position_start_wall_follow = None

    def close(self):
        self.closed = True

    def launch(self):
        now = None
        while not self.closed:
            if now:
                print (time() - now) * 1000
            now = time()
            new_task = self.task_change
            self.task_change = False
            # self.plot_pf_index = 0

            # odometry update
            previous_x, previous_y, previous_th = (
                self.odometry.x, self.odometry.y, self.odometry.th
            )
            self.odometry.update(now, self.conn.read_counts())
            delta_pose = (
                self.odometry.x - previous_x,
                self.odometry.y - previous_y,
                (self.odometry.th - previous_th) % (2 * pi)
            )

            # ir distances update
            ir = self.conn.read_ir()
            for direction in Sensor.DIRECTIONS:
                self.ir_distances[direction] = self.ir2cm.ir2cm(ir, direction)

            # particle filter update
            if self.do_localisation:
                # self.particle_filter.step(delta_pose, [
                #     self.ir_distances["left"], self.ir_distances["front"],
                #     self.ir_distances["right"], self.ir_distances["back"]],
                #                           mode="wave")
                # output = ""
                # for direction in Sensor.DIRECTIONS:
                #     output += (direction + ": " + str(self.ir_distances[direction])
                #                + "cm, ")
                # print(output[:-2])

                # self.localisation['x'], self.localisation['y'],\
                    # self.localisation['th'] = self.particle_filter.get_pose()

                # if self.particle_filter.just_localised:
                #     delta = np.zeros((1, 2))
                #     delta = self.particle_filter.get_pose()
                #     self.home += delta
                #     for i in range(len(self.food_locations)):
                #         self.food_locations[i] += delta

                self.localisation['x'], self.localisation['y'],\
                    self.localisation['th'] = \
                    self.odometry.x, self.odometry.y, self.odometry.th

                # self.plotter.update_particles()
            else:
                self.localisation['x'], self.localisation['y'],\
                    self.localisation['th'] = \
                    self.odometry.x, self.odometry.y, self.odometry.th

            if self.task[0:10] == "calibrate ":
                if new_task:
                    self.do_localisation = False
                    self.calibration_x = 0
                    self.odometry.clear()
                calibration_direction = self.task[10:]
                self.autocalibrate(calibration_direction)

            # practical 3 task
            elif self.task == "collect food":
                if self.has_food:
                    self.set_desired_location(
                        self.food_locations.closest((self.localisation['x'], self.localisation['y']),
                                                    home=self.home))
                    if self.desired_location == self.home:
                        self.print_once("going home")
                        if self.arrived():
                            self.print_once("arrived home")
                            # drop food
                            self.has_food = False
                            self.conn.stop()
                            self.conn.flash_leds(iterations=3)
                            self.food_locations.replenish()
                    else:
                        self.print_once("getting food at " + str(self.desired_location) + " on the way home")
                        if self.arrived():
                            self.print_once("arrived at food location")
                            self.pickup_food()

                elif self.food_locations.any():
                    # go to closest food location
                    self.set_desired_location(
                        self.food_locations.closest((self.localisation['x'], self.localisation['y'])))
                    self.print_once("going back to food location " + str(self.desired_location))
                    if self.arrived():
                        self.print_once("arrived at food location")
                        self.pickup_food()
                else:
                    self.print_once("exploring")
                    self.subsumption(self.explore)
                    # TODO better exploration algorithm here

            elif new_task:
                self.set_desired_location(None)

                # task 3
                if self.task == "3":
                    print("[ROBOT] starting task 3")
                    if self.algorithm == "":
                        self.print_once("[WARNING] no algorithm set")
                    self.odometry.clear()
                    self.timers = {'start': now}
                    self.food_locations = FoodLocations()
                    self.has_food = False
                    self.conn.set_led(0, 0)
                    self.set_task("collect food")
                elif self.task == "":
                    self.food_locations.add(
                        (self.localisation['x'], self.localisation['y']))
                    self.pickup_food()
                    print("[ROBOT] food found at " + str((self.localisation['x'], self.localisation['y'])))
                    self.set_task("collect food")

                # direct movement
                elif self.task == " ":
                    self.conn.stop()
                elif self.task == "w":
                    self.conn.go(self.speed_forward)
                elif self.task == "s":
                    self.conn.go(-self.speed_forward)
                elif self.task == "d":
                    self.conn.turn(self.speed_turn, -self.speed_turn)
                elif self.task == "a":
                    self.conn.turn(-self.speed_turn, self.speed_turn)

                # display
                elif self.task == "c":
                    self.odometry.clear()
                elif self.task == "l":
                    self.do_localisation = not self.do_localisation

                # set algorithm
                elif self.task == "bug0":
                    self.algorithm = "bug0"
                    self.subtask = "initial turn"
                elif self.task == "astar":
                    self.algorithm = "astar"

                elif self.task == "square":
                    # Move in a square-like pattern to test odometry
                    TIME_TO_90_TURN = 1.69
                    TIME_TO_GO = 5
                    TIME_TO_STOP = 0.5

                    for _ in xrange(4):
                        self.conn.turn(-3, 3)
                        self.odometry.update(time(), self.conn.read_counts())
                        sleep(TIME_TO_90_TURN)

                        self.conn.stop()
                        self.odometry.update(time(), self.conn.read_counts())
                        sleep(TIME_TO_STOP)

                        self.conn.go(3)
                        self.odometry.update(time(), self.conn.read_counts())
                        sleep(TIME_TO_GO)

                        self.conn.stop()
                        self.odometry.update(time(), self.conn.read_counts())
                        sleep(TIME_TO_STOP)

                    print "eh"
                    self.task = " "

                else:
                    self.print_once("[ERR] unrecognised command")

            # execute algorithms
            u = self.untangling
            self.untangling = self.untangling or self.is_tangled()
            if self.untangling != u:
                try:
                    if self.utime:
                        pass
                except Exception:
                    self.utime = time()

            if self.untangling:
                self.print_once("untangling")
                self.untangle()
            elif self.desired_location is not None and not self.arrived():
                if self.algorithm == "bug0":
                    desired_direction = Util.get_direction(
                        (self.localisation['x'], self.localisation['y']), self.desired_location)
                    if self.subtask == "initial turn":
                        self.position_start_wall_follow = None
                        difference = Util.get_turn_angle(degrees(self.localisation['th']), desired_direction['angle'])
                        if abs(difference) > self.angle_threshold(desired_direction['distance']):
                            self.go_in_direction(desired_direction, and_forward=False)
                        else:
                            self.subtask = "bug0"
                            print("initial turn finished")
                    elif self.subtask == "bug0":
                        self.bug0(desired_direction)

                elif self.algorithm == "astar":
                    # sublocation =
                    pass
                else:
                    self.print_once("[ERR] no algorithm set")

    def pickup_food(self):
        self.conn.stop()
        self.conn.set_led(0, 1)
        sleep(1)
        self.has_food = True
        self.food_locations.eat(self.desired_location)

    def set_desired_location(self, desired_location):
        # print(self.desired_location, desired_location)
        if self.desired_location != desired_location:
            self.desired_location = desired_location
            self.subtask = "initial turn"

    def set_task(self, task):
        if task != self.task:
            self.task = task
            self.task_change = True

    def print_once(self, print_str):
        if print_str != self.print_str:
            print("[ROBOT] " + print_str)
            self.print_str = print_str

    def is_tangled(self):
        return self.odometry.th < -6 * pi or 6 * pi < self.odometry.th

    def untangle(self):
        if self.odometry.th > 3 * pi:
            self.conn.turn(self.speed_turn, -self.speed_turn)
        elif self.odometry.th < -3 * pi:
            self.conn.turn(-self.speed_turn, self.speed_turn)
        else:
            print("[ROBOT] finished untangling")
            self.conn.stop()
            self.untangling = False

            print time() - self.utime
            del self.utime

    def arrived(self):
        return Util.get_direction(
            (self.localisation['x'], self.localisation['y']),
            self.desired_location
        )['distance'] <= self.distance_threshold

    @profile
    def autocalibrate(self, calibration_direction):
        if calibration_direction == "front" or calibration_direction == "back":
            self.autocalibrate_straight(calibration_direction)
        elif calibration_direction == "left" or calibration_direction == "right":
            self.autocalibrate_side(calibration_direction)
        else:
            self.print_once("invalid calibration direction '"
                            + calibration_direction + "'")

    @profile
    def autocalibrate_straight(self, calibration_direction):
        if calibration_direction == "front":
            calibration_point = (-Ir2Cm.distances[self.calibration_x] * 10, 0)
        else:  # calibration_direction == "back":
            calibration_point = (Ir2Cm.distances[self.calibration_x] * 10, 0)
        distance = abs(self.odometry.x - calibration_point[0])
        direction = Util.get_direction(
            (self.odometry.x, self.odometry.y), calibration_point)

        if distance <= 0.5:
            self.conn.stop()
            sleep(0.5)
            profile_other(self.calibration.autocalibrate, self, calibration_direction, self.calibration_x)
            # self.calibration.autocalibrate(
            #     calibration_direction, self.calibration_x)
            self.calibration_x += 1
            if self.calibration_x >= len(Ir2Cm.distances):
                self.calibration_x = 0
                self.odometry.clear()
                self.task = " "

                print "########## Robot ##########"
                print_profiler(self)
                print "####### Calibration #######"
                print_profiler(self.calibration)
        else:
            difference = Util.get_turn_angle(
                degrees(self.odometry.th), direction['angle'])
            if -90 <= difference <= 90:
                self.conn.go(1)
            else:
                self.conn.go(-1)

    @profile
    def autocalibrate_side(self, calibration_direction):
        # TODO - currently not as accurate as manual calibration
        if calibration_direction == "left":
            calibration_point = (0, -Ir2Cm.distances[self.calibration_x] * 10)
            bearing = -90
        else:  # calibration_direction == "right":
            calibration_point = (0, Ir2Cm.distances[self.calibration_x] * 10)
            bearing = 90
        distance = abs(self.odometry.y - calibration_point[1])
        direction = Util.get_direction(
            (self.odometry.x, self.odometry.y), calibration_point)

        if distance <= 0.5:
            # self.print_once("within")
            if degrees(self.odometry.th) >= 2:
                self.conn.turn(1, -1)
            elif degrees(self.odometry.th) <= -2:
                self.conn.turn(-1, 1)
            else:
                self.conn.stop()
                sleep(0.5)
                self.calibration.autocalibrate(
                    calibration_direction, self.calibration_x)
                self.calibration_x += 1
                if self.calibration_x >= len(Ir2Cm.distances):
                    self.calibration_x = 0
                    self.odometry.clear()
                    self.task = " "
        elif degrees(self.odometry.th) >= bearing + 2:
            # self.print_once("turning to move")
            self.conn.turn(1, -1)
        elif degrees(self.odometry.th) <= bearing - 2:
            # self.print_once("turning to move")
            self.conn.turn(-1, 1)
        else:
            # self.print_once("moving")
            difference = Util.get_turn_angle(
                degrees(self.odometry.th), direction['angle'])
            if -90 <= difference <= 90:
                self.conn.go(1)
            else:
                self.conn.go(-1)

    def subsumption(self, top_level, *args):
        is_obstacle = self.is_obstacle()
        wall_direction, wall_distance = self.wall_direction()

        if is_obstacle:
            self.print_once("[OBST]")
            self.avoid_obstacle(wall_direction, wall_distance)
        elif wall_distance <= self.wall_start_follow_distance:
            self.print_once("[WALL] " + str(wall_distance))
            self.follow_wall(wall_direction, wall_distance)
        else:
            self.print_once("[TOP]")
            top_level(*args)

    def bug0(self, direction_to_home):
        is_obstacle = self.is_obstacle()
        wall_direction, wall_distance = self.wall_direction()

        if is_obstacle:
            self.position_start_wall_follow = None
            self.print_once("[OBST]")
            self.avoid_obstacle(wall_direction, wall_distance)
        elif wall_distance <= self.wall_start_follow_distance:
            if self.position_start_wall_follow is None:
                self.print_once("[RESET WALL]")
                self.position_start_wall_follow = \
                    (self.localisation['x'], self.localisation['y'])
            elif Util.get_direction(
                    (self.localisation['x'], self.localisation['y']),
                    self.position_start_wall_follow)["distance"] < 100:
                self.print_once("[WALL]")
                self.follow_wall(wall_direction, wall_distance)
            else:
                self.print_once("[TURN TO DESIRED LOCATION]")
                self.subtask = "initial turn"
        else:
            self.position_start_wall_follow = None
            self.print_once("[GO TO DESIRED LOCATION]")
            self.go_in_direction(direction_to_home)

    def is_obstacle(self):
        return self.check_detection("front")

    def check_detection(self, direction):
        distance = self.ir_distances[direction]
        if distance <= self.obstacle_threshold:
            return True
        else:
            return False

    def wall_direction(self):
        left_distance = self.ir_distances["left"]
        right_distance = self.ir_distances["right"]
        if left_distance < right_distance:
            return Direction.LEFT, left_distance
        elif right_distance < left_distance:
            return Direction.RIGHT, right_distance
        else:
            return Direction.random(), left_distance

    def avoid_obstacle(self, wall_direction, wall_distance):
        if self.turn_direction == Direction.NONE:
            if wall_distance <= self.wall_start_follow_distance:
                self.turn_direction = -wall_direction
                # self.print_once(
                #     '[OBST] turning ' + Direction.decode(self.turn_direction)
                #     + ' away from object and wall'
                # )
            else:
                self.turn_direction = Direction.random()
                # self.print_once(
                #     '[OBST] turning ' + Direction.decode(self.turn_direction)
                #     + ' randomly away from object'
                # )
        self.conn.turn(-self.speed_turn * self.turn_direction,
                       self.speed_turn * self.turn_direction)

    def follow_wall(self, wall_direction, wall_distance):
        self.turn_direction = Direction.NONE
        # self.print_once('[WALL] following wall on ' +
        #                 Direction.decode(wall_direction))
        if wall_distance != self.wall_follow_distance:
            if wall_distance > self.wall_follow_distance:
                lean_direction = wall_direction
                # self.print_once(
                #     "[WALL] turning " + Direction.decode(lean_direction) +
                #     ", towards wall")
            else:
                lean_direction = -wall_direction
                # self.print_once(
                #     "[WALL] turning " + Direction.decode(lean_direction) +
                #     ", away from wall")
            lean_left = lean_direction == Direction.LEFT
            lean_right = lean_direction == Direction.RIGHT
            error = abs(wall_distance - self.wall_follow_distance)
            self.conn.turn(
                self.speed_forward + lean_right * error * self.correction_factor,
                self.speed_forward + lean_left * error * self.correction_factor
            )
        else:
            # self.print_once("[WALL] following at correct distance")
            pass

    def explore(self):
        self.turn_direction = Direction.NONE
        self.print_once('[EXPLORE] going forward')
        self.conn.go(self.speed_forward)

    def go_in_direction(self, direction, and_forward=True):
        """
        Goes to a certain position, specified by the desired direction to be in
        """
        difference = Util.get_turn_angle(degrees(self.localisation['th']), direction['angle'])
        # self.print_once("[GOAL] turning towards goal")
        if abs(difference) > self.angle_threshold(direction['distance']):
            if difference > 0:
                self.conn.turn(-self.speed_turn, self.speed_turn)
            else:
                self.conn.turn(self.speed_turn, -self.speed_turn)
        else:
            if and_forward:
                # self.print_once("[GOAL] going forward towards goal")
                self.conn.go(self.speed_forward)

    @staticmethod
    def angle_threshold(distance):
        angle_threshold = round(((30. - 5.)/(1000. - 100.)) * (distance - 100) + 5)
        if angle_threshold < 5:
            angle_threshold = 5
        elif angle_threshold > 40:
            angle_threshold = 40
        # print(distance, angle_threshold)
        return angle_threshold
