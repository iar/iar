from time import time
from math import sin, cos, pi

import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np

from particle_filter_2 import ParticleFilter, print_profiler, IMAGE_SCALE


class PfPlotter:
    def __init__(self, particle_filter, filename='map_images/arena_masked.jpg'):
        self.img = mpimg.imread(filename)
        self.replot = False
        self.pf = particle_filter

    def launch(self):
        while True:
            if self.replot:
                self._plot_from_particles()

    def plot(self):
        self.replot = True

    def _plot_from_particles(self):
        self.replot = False
        t = time()

        plt.figure()
        image = plt.imshow(self.img)
        positions = []
        side_coords = []
        for p in self.pf.particles:
            positions.append((p.x, p.y))
            side_c = []
            for side in [-.5 * pi, 0, .5 * pi]:
                sensor_orient = (p.orient + side) % (2. * pi)
                cast_dist = self.pf.raycast(p.x, p.y, sensor_orient)
                dx = p.x + cast_dist * cos(sensor_orient)
                dy = p.y + cast_dist * sin(sensor_orient)

                side_c.append((dx, dy))
            side_coords.append(side_c)

        # Place the dots
        plt.scatter(x=np.array(positions)[:, 0], y=np.array(positions)[:, 1], c='g', s=40)
        plt.scatter(x=np.array(positions)[:, :, 0].transpose(), y=np.array(positions)[:, :, 1].transpose(), c='r', s=10)

        # Connect the dots
        for sc in side_coords:
            plt.plot(
                [(positions[0], sc[0][0]), (positions[0], sc[1][0]), (positions[0], sc[2][0])],
                [(positions[1], sc[0][1]), (positions[1], sc[1][1]), (positions[1], sc[2][1])],
                c='r'
            )

        likely_pose = self.pf.get_pose(convert_back=False)
        plt.scatter(x=likely_pose[0], y=likely_pose[1], c='b', s=40)
        plt.plot(
            [likely_pose[0], likely_pose[0] + 10. * cos(likely_pose[2])],
            [likely_pose[1], likely_pose[1] + 10. * sin(likely_pose[2])],
            c='b'
        )

        plt.show()
        print "Plotted in %f" % (time() - t)


if __name__ == "__main__":
    # pf = ParticleFilter()
    # for x, y, th in pf.generate_by_gaussian(10, [100, 100, 2.0], 10.):
    #     print x, y, th, ParticleFilter.Particle(pf, x, y, th, 1./10)
    #
    # img = mpimg.imread('map_images/arena_clear.jpg')
    # imgplot = plt.imshow(img)
    #
    # plt.show()


    # number_of_runs = 10
    #
    # img = mpimg.imread('map_images/arena_masked.jpg')
    # imgplot = plt.imshow(img)
    #
    # t = time()
    # pf = ParticleFilter()
    # positions = []
    # for r in xrange(number_of_runs):
    #     try:
    #         # Generate values
    #         x = random() * pf.map_shape[1]
    #         y = random() * pf.map_shape[0]
    #
    #         # Ignore if placed on object. This prevents most placements outside grid.
    #         if pf.map[int(y)][int(x)] > GRID_THRESHOLD:
    #             theta = random() * 2. * pi
    #
    #             # Raycast and calculate positions
    #             cast_dist = pf.raycast(x, y, theta)
    #             dx = x + cast_dist * cos(theta)
    #             dy = y + cast_dist * sin(theta)
    #
    #             positions.append([(x, y), (dx, dy)])
    #
    #     except Exception as e:
    #         raise Exception(e.message + ". exception at %d" % r)
    # print "Executed Particle Filter in %f" % (time() - t)
    #
    # plt.scatter(x=np.array(positions)[:, 0, 0], y=np.array(positions)[:, 0, 1], c='g', s=40)
    # plt.scatter(x=np.array(positions)[:, 1, 0], y=np.array(positions)[:, 1, 1], c='r', s=10)
    #
    # # I am sorry.
    # plt.plot(np.array(positions)[:, :, 0].transpose(), np.array(positions)[:, :, 1].transpose(), c='r')
    #
    # plt.show()

    img = mpimg.imread('map_images/arena_masked_resized.jpg')
    imgplot = plt.imshow(img, cmap="gray")

    pf = ParticleFilter()
    for _ in xrange(2):
        pf.step([50., 50., 2.], [6, 6, 4, 4])

    print_profiler(pf)

    plt.scatter(x=np.array(pf.particles)[:, 0], y=np.array(pf.particles)[:, 1], c='g', s=40)
    plt.show()
