#!/usr/bin/env python

from control import *

@with_connection
def main(conn):
    conn.stop()

if __name__ == "__main__":
    main()
