from heapq import *
from math import sin, cos, radians
import numpy as np

# Maximum distance at which the robot sees obstacles.
SENSOR_DISTANCE = 60

# Sensors-related variables, for better map object position estimation
KHEPERA_RADIUS_MM = 27.5

# Angles of [left, front, right] sensors, on the robot.
SENSOR_ANGLES = [180, 90, 0]

# ARENA_SIZE, mm
ARENA_SIZE = (1400, 760)

# Grid size, in mm
GRID_SIZE = 50


class Mapper:
    SPACE = 0
    OBJECT = 1

    # Grid shape
    GRID_SHAPE = ((max(ARENA_SIZE) + 1) * 2 / GRID_SIZE, (max(ARENA_SIZE) + 1) * 2 / GRID_SIZE)

    def __init__(self, plotter):
        """
        Mapper, responsible for mapping the environment and figuring out the best path to return.
        """
        self.plotter = plotter
        self.grid = np.zeros(self.GRID_SHAPE)

    def mm_to_grid(self, pos):
        """
        :param pos: (x, y) - coordinates of a point, in mm
        :return: (x_grid, y_grid) - grid cell of a point.
        """
        return pos[0] / GRID_SIZE + GRID_SIZE / 2, pos[1] / GRID_SIZE + GRID_SIZE / 2

    def flag_object(self, pos):
        """
        Flags a grid cell with given cm coordinates as being obstructed.
        :param pos: (x, y) - coordinates of a point, in mm
        """
        grid_pos = self.mm_to_grid(pos)
        if 0 < grid_pos[0] < len(self.grid[0]) and 0 < grid_pos[1] < len(self.grid):
            self.grid[grid_pos] = self.OBJECT

    def flag_clear(self, pos):
        """
        Flags the cell as being clear
        :param pos: (x, y) - coordinates of a point, in mm
        """
        grid_pos = self.mm_to_grid(pos)
        if 0 < grid_pos[0] < len(self.grid[0]) and 0 < grid_pos[1] < len(self.grid):
            self.grid[grid_pos] = self.SPACE

    def generate_update(self, my_x, my_y, my_th, distances):
        """
        Takes in distances to objects in mm, for each sensor and updates the map with its values.
        :param my_x: robot center x coordinate, mm
        :param my_y: robot center y coordinate, mm
        :param my_th: robot orientation, degrees
        :param distances: Sensor distances (in mm) in the order: [left, front, right].
                          It is assumed that sensors are facing the sides at a 90 degree angle.
        :return: add_list [(float, float), ...] and delete_list [(float, float), ...] of objects spotted.
        """

        if len(distances) != 3:
            print("ERROR: Distance array must always contain exactly 3 floats")
        else:
            add_list = []
            delete_list = []
            for d in xrange(3):
                sensor_distance = SENSOR_DISTANCE

                # equivalent to "if object detected"
                if distances[d] <= sensor_distance:
                    x_obj = my_x + cos(radians(SENSOR_ANGLES[d] + my_th)) * (KHEPERA_RADIUS_MM + distances[d])
                    y_obj = my_y + sin(radians(SENSOR_ANGLES[d] + my_th)) * (KHEPERA_RADIUS_MM + distances[d])
                    add_list.append((x_obj, y_obj))
                    sensor_distance = int(distances[d])

                x_list = map(lambda d0: my_x + int(cos(radians(SENSOR_ANGLES[d] + my_th)) * d0),
                             xrange(sensor_distance))
                y_list = map(lambda d0: my_y + int(sin(radians(SENSOR_ANGLES[d] + my_th)) * d0),
                             xrange(sensor_distance))

                delete_list = set(zip(x_list, y_list)).difference(add_list)

            return add_list, delete_list

    def apply_update(self, add_list, delete_list):
        """
        Applies all given update and delete lists to add / remove objects from the map.
        :param add_list: add_list [(float, float), ...], in mm
        :param delete_list: delete_list [(float, float), ...], in mm
        :return: add_list_round [(float, float), ...] and delete_list_round [(float, float), ...] , in mm, but rounded
                 to grid square coordinates.
        """

        # TODO: Ugh, I feel there's a good numpy method for this that I forgot. Might be a bottleneck.
        for delete in delete_list:
            self.flag_clear(delete)
        for add in add_list:
            self.flag_clear(add)

        return map(lambda pos: (int(round(pos[0])) / GRID_SIZE * GRID_SIZE, int(round(pos[1])) / GRID_SIZE * GRID_SIZE), add_list), \
               map(lambda pos: (int(round(pos[0])) / GRID_SIZE * GRID_SIZE, int(round(pos[1])) / GRID_SIZE * GRID_SIZE), delete_list)

    # TODO: plot
    def path_smoothing(self, astar_path, coefficient):
        """
        Smoothens the path that A* generates.
        :param astar_path: - [[x, y]]
        :param coefficient: smoothing coefficient - more => smoother, but more corner cutting too.
        :return: [[x_smooth, y_smooth]]
        """
        print "A* length:", len(astar_path)
        smooth_path = np.copy(astar_path)
        term1 = np.concatenate((np.array([[0, 0]]), astar_path[:-1]))
        term2 = np.array(astar_path)
        term3 = np.concatenate((astar_path[1:], np.array([[0, 0]])))
        return smooth_path - coefficient * (term1 - term2 + term3)

    ########################################################################################
    #  A* Algorithm provided made by Christian Careaga (christian.careaga7@gmail.com)      #
    #  http://code.activestate.com/recipes/578919-python-a-pathfinding-with-binary-heap/   #
    ########################################################################################
    def heuristic(self, a, b):
        return (b[0] - a[0]) ** 2 + (b[1] - a[1]) ** 2

    def astar(self, start, goal):
        neighbors = [(0, 1), (0, -1), (1, 0), (-1, 0), (1, 1), (1, -1), (-1, 1), (-1, -1)]

        close_set = set()
        came_from = {}
        gscore = {start: 0}
        fscore = {start: self.heuristic(start, goal)}
        oheap = []

        heappush(oheap, (fscore[start], start))

        while oheap:

            current = heappop(oheap)[1]

            if current == goal:
                data = []
                while current in came_from:
                    data.append(current)
                    current = came_from[current]
                return data

            close_set.add(current)
            for i, j in neighbors:
                neighbor = current[0] + i, current[1] + j
                tentative_g_score = gscore[current] + self.heuristic(current, neighbor)
                if 0 <= neighbor[0] < self.grid.shape[0]:
                    if 0 <= neighbor[1] < self.grid.shape[1]:
                        if self.grid[neighbor[0]][neighbor[1]] == 1:
                            continue
                    else:
                        # array bound y walls
                        continue
                else:
                    # array bound x walls
                    continue

                if neighbor in close_set and tentative_g_score >= gscore.get(neighbor, 0):
                    continue

                if tentative_g_score < gscore.get(neighbor, 0) or neighbor not in [i[1] for i in oheap]:
                    came_from[neighbor] = current
                    gscore[neighbor] = tentative_g_score
                    fscore[neighbor] = tentative_g_score + self.heuristic(neighbor, goal)
                    heappush(oheap, (fscore[neighbor], neighbor))

        return False


        # nmap = np.array([
        #     [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        #     [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1],
        #     [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        #     [1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
        #     [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        #     [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1],
        #     [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        #     [1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
        #     [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        #     [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1],
        #     [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]])
        #
        # print self.astar(nmap, (0, 0), (10, 13))
        # h = self.astar(nmap, (0, 0), (10, 13))
        # mapper = Mapper()
        # print mapper.path_smoothing(h, 0.2)
