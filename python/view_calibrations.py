#!/usr/bin/env python

from __future__ import print_function

import numpy as np

from calibration import Sensor


def main():
    calibrations = {}
    for direction in Sensor.DIRECTIONS:
        calibrations[direction] = np.load("directions/" + direction + ".npy")
        print(direction + ":")
        print(calibrations[direction])
        print()

if __name__ == "__main__":
    main()
