#!/usr/bin/env python

from time import sleep

import numpy as np
from class_profiler import *
from control import with_connection


class Sensor(object):
    WALL_LEFT = 0
    SIDE_LEFT = 1
    FRONT_LEFT = 2
    FRONT_RIGHT = 3
    SIDE_RIGHT = 4
    WALL_RIGHT = 5
    BACK_RIGHT = 6
    BACK_LEFT = 7

    @staticmethod
    def decode(sensor):
        if sensor == 0:
            return "WALL_LEFT"
        elif sensor == 1:
            return "SIDE_LEFT"
        elif sensor == 2:
            return "FRONT_LEFT"
        elif sensor == 3:
            return "FRONT_RIGHT"
        elif sensor == 4:
            return "SIDE_RIGHT"
        elif sensor == 5:
            return "WALL_RIGHT"
        elif sensor == 6:
            return "BACK_RIGHT"
        elif sensor == 7:
            return "BACK_LEFT"

    FRONT = [FRONT_LEFT, FRONT_RIGHT]
    LEFT = [SIDE_LEFT, WALL_LEFT]
    RIGHT = [SIDE_RIGHT, WALL_RIGHT]
    BACK = [BACK_RIGHT, BACK_LEFT]
    ALL = [WALL_LEFT, SIDE_LEFT, FRONT_LEFT, FRONT_RIGHT, SIDE_RIGHT,
           WALL_RIGHT, BACK_RIGHT, BACK_LEFT]

    @staticmethod
    def direction_to_sensors(direction):
        if direction == "front":
            return Sensor.FRONT
        elif direction == "left":
            return Sensor.LEFT
        elif direction == "right":
            return Sensor.RIGHT
        elif direction == "back":
            return Sensor.BACK

    DIRECTIONS = ["front", "left", "right", "back"]


class Ir2Cm(object):
    distances = [0.0, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0, 4.5, 5.0, 6.0,
                 7.0, 8.0]

    def __init__(self):
        self.calibrations = {}
        for direction in Sensor.DIRECTIONS:
            self.calibrations[direction] = np.load("directions/" + direction + ".npy")

    def ir2cm(self, ir, direction):
        sensors = Sensor.direction_to_sensors(direction)
        ir = np.array(ir)[sensors]

        closest_value = float("inf")
        distance = Ir2Cm.distances[-1]
        for x in range(len(Ir2Cm.distances)):
            if x >= len(self.calibrations[direction]):
                pass
            else:
                cm = Ir2Cm.distances[x]
                value = np.linalg.norm(ir - self.calibrations[direction][x, :])
                if value < closest_value:
                    closest_value = value
                    distance = cm
        return distance

class Calibration(object):
    def __init__(self, conn, iterations):
        self.conn = conn
        self.iterations = iterations
        self.sleep_time = 0.1
        self.calibrations = None
        self.profiler = dict()

    def get_average_ir(self, conn, sensors, iterations, sleep_time):
        ir_values = np.zeros((iterations, len(sensors)))
        for x in xrange(iterations):
            ir_values[x, :] = np.array(profile_other(conn.read_ir, self))[sensors]
            sleep(sleep_time)
        return ir_values.sum(axis=0) / iterations

    def calibrate(self, direction):
        sensors = Sensor.direction_to_sensors(direction)
        calibrations = np.zeros((len(Ir2Cm.distances), len(sensors)))
        for x in range(len(Ir2Cm.distances)):
            cm = Ir2Cm.distances[x]
            input_go(cm)
            calibrations[x, :] = self.get_average_ir(
                self.conn, sensors, self.iterations, self.sleep_time)
            print(calibrations[x, :])
        print(calibrations)
        np.save("directions/" + direction + ".npy", calibrations)
        return 1

    def autocalibrate(self, direction, x):
        sensors = Sensor.direction_to_sensors(direction)
        if x == 0:
            self.calibrations = np.zeros((len(Ir2Cm.distances), len(sensors)))

        self.calibrations[x, :] = self.get_average_ir(
            self.conn, sensors, self.iterations, self.sleep_time)
        print(self.calibrations[x, :])

        if x == len(Ir2Cm.distances) - 1:
            print("saving...")
            print(self.calibrations)
            np.save("directions/" + direction + ".npy", self.calibrations)
            print("saved")


def input_bool(prompt):
    print(prompt + " (y/n)")
    response = None
    while response != "y" and response != "n":
        response = raw_input()
        if response == "y":
            return True
        else:
            return False


def input_go(cm):
    print(str(cm) + "cm, ready? (press enter)")
    raw_input()


@with_connection
def main(conn):
    conn.verbose = False

    iterations = 5
    calibration = Calibration(conn, iterations)
    for direction in Sensor.DIRECTIONS:
        if input_bool("calibrate from " + direction + "?"):
            calibration.calibrate(direction)


if __name__ == "__main__":
    main()
