"""
Command_logger singleton. Not in a class, as it needs to be accessed from both Robot and Control, when robot is using Control
already. I also wanted to hide the logging of commands from Robot and implement that in Control, when the commands
are sent to the motors.
"""

class CommandLogger(object):
    def __init__(self):
        self.enabled = False
        self.cmd_log = []
        self.prev_cmd = None
        self.start_time = None

    def register_cmd(self, left, right, now):
        """
        Adds the command to the command log.
        :param left: left wheel speed
        :param right: right wheel speed
        :param now: approximate time the command was RECEIVED
        """
        if self.enabled:
            if self.prev_cmd:
                self.cmd_log.append(self.prev_cmd + (now - self.start_time,))
                # print(self.cmd_log)

            if self.prev_cmd != (0, 0):
                self.prev_cmd = (left, right)
                self.start_time = now

    def clear_log(self):
        self.cmd_log = []
