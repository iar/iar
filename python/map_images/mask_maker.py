import cv2
import numpy as np
from particle_filter_2 import IMAGE_SCALE, GRID_THRESHOLD


class MaskMaker:
    def __init__(self):
        pass

    @staticmethod
    def make_array(root, input_file="arena_masked.jpg", output_file="arena_masked.npy", show=False):
        """
        Takes in a file and creates a numpy array, with 0 for black pixels and 255 otherwise. The array is saved
        as a file.

        :param root:          Root folder of the files.
        :param input_file:    Path to the photo. Black zones correspond to objects.
        :param output_file:   File to save the mask as.
        :param show:          Show resulting threshold
        """
        img = cv2.imread(root + input_file, cv2.IMREAD_GRAYSCALE)

        # Resize image
        height, width = img.shape[:2]
        res = cv2.resize(img, (int(IMAGE_SCALE * width), int(IMAGE_SCALE * height)))
        cv2.imwrite(root + "arena_masked_resized.jpg", res)

        _, thresh = cv2.threshold(res, GRID_THRESHOLD, 255, cv2.THRESH_BINARY)

        print thresh.shape
        np.save(root + output_file, thresh)

        print "Mask made successfully."

        if show:
            cv2.imshow("", thresh)
            cv2.waitKey(0)


if __name__ == "__main__":
    MaskMaker.make_array("", show=False)
