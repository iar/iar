#!/usr/bin/env python

from __future__ import print_function

import serial
from time import time

from command_logger import *


class Connection:
    def __init__(self, command_logger):
        self.conn = None
        self.verbose = True
        self.move_enabled = True
        self.command_logger = command_logger
        self.command_logger.enabled = True

    def connect(self, port="/dev/ttyS0", baudrate=9600, stopbits=2, timeout=1,
                **kwargs):
        print("[CONN] connecting...")
        self.conn = serial.Serial(port=port, baudrate=baudrate,
                                  stopbits=stopbits, timeout=timeout, **kwargs)
        if not self.conn.isOpen():
            self.conn.open()

    def disconnect(self):
        print("[CONN] disconnecting...")
        if self.conn.isOpen():
            self.conn.close()
        print("[CONN] disconnected")

    def test_connection(self):
        counts = self.read_counts()
        if counts == -1:
            return False
        else:
            return True

    def send_command(self, command):
        # we should check if we have built up a backlog of serial messages from
        # the Khepera. If there is a backlog, we should read out of the serial
        # buffer so that future communications aren't messed up.
        if self.conn.inWaiting() > 0:
            if self.verbose:
                print("WARNING! Messages were waiting to be read!")
                print("This may be indicative of a problem elsewhere in your "
                      "code.")
            while self.conn.inWaiting() > 0:
                message = self.conn.readline()[:-1]
                if self.verbose:
                    print(message)

        # we must make sure that the command string is followed by a newline
        # character before sending it to the Khepera
        if command[-1] != "\n":
            command += "\n"
        self.conn.write(command)

        # we check for a response from the Khepera
        answer = self.conn.readline()

        # now we can check if the response from the Khepera matches our
        # expectations
        if self.verbose:
            print("SENT     : " + command[:-1])
            print("RECEIVED : " + answer[:-1])
            if len(answer) < 1:
                print("WARNING! No response received!")
            elif answer[0] != command[0].lower():
                print("WARNING! Response does not match issued command!")
        return answer

    def set_speeds(self, left, right):
        if self.move_enabled:
            self.send_command("D," + str(int(left)) + "," + str(int(right)))
            now = time()
            self.command_logger.register_cmd(left, right, now)

    def go(self, speed):
        self.set_speeds(speed, speed)

    def stop(self):
        self.go(0)

    def turn(self, left, right):
        self.set_speeds(left, right)

    def move_by_count(self, left_counts, right_counts):
        print("Moving by %f, %f. Note that this is not counted by command_logger" % (left_counts, right_counts))
        self.send_command("C," + str(int(left_counts)) + "," + str(int(right_counts)))

    def set_counts(self, left_count, right_count):
        self.send_command("G," + str(left_count) + "," + str(right_count))

    def flash_leds(self, iterations):
        from time import sleep
        for _ in range(iterations):
            self.set_led(0, 0)
            self.set_led(1, 0)
            sleep(0.25)
            self.set_led(0, 1)
            self.set_led(1, 1)
            sleep(0.25)
        self.set_led(0, 0)
        self.set_led(1, 0)

    def set_led(self, led_number, action_number):
        led_number = int(led_number)
        action_number = int(action_number)
        if led_number < 0 or 1 < led_number:
            print("[ERR] invalid led_number '" + str(led_number))
        elif action_number < 0 or 2 < action_number:
            print("[ERR] invalid action_number '" + str(action_number))
        else:
            self.send_command("L," + str(led_number) + "," + str(action_number))

    @staticmethod
    def _parse_sensor_string(sensor_string):
        if len(sensor_string) < 1:
            return -1
        else:
            # we need to remove some superfluous characters in the returned
            # message
            sensor_string = sensor_string[2:-2]
            # and cast the comma separated sensor readings to integers
            sensor_values = [int(ss) for ss in sensor_string.split(",")]
            return sensor_values

    def read_ir(self):
        while True:
            ir_string = self.send_command("N")
            try:
                sensors_values = self._parse_sensor_string(ir_string)
                if sensors_values == -1 or len(sensors_values) != 8:
                    print("[ERR] read_ir: connection error")
                    sensors_values = [0]*8
                return sensors_values
            except ValueError:
                pass

    def read_ambient(self):
        ambient_string = self.send_command("O")
        return self._parse_sensor_string(ambient_string)

    def read_counts(self):
        count_string = self.send_command("H")
        counts = self._parse_sensor_string(count_string)
        if counts == -1:
            print("[ERR] read_counts: connection error")
            counts = [0, 0]
        return counts


def with_connection(func):
    def connect_and_call_func(*args, **kwargs):
        command_logger = CommandLogger()
        conn = Connection(command_logger)
        conn.connect()

        try:
            func(conn, *args, **kwargs)
        except KeyboardInterrupt:
            print("Stopping the robot")

        conn.stop()
        conn.disconnect()
        print("Finished")

    return connect_and_call_func


class MockConnection(object):
    def __init__(self, command_logger):
        self.verbose = True
        self.move_enabled = True
        self.command_log_enabled = True
        self.command_logger = command_logger
        self.command_logger.enabled = True

    def connect(self, port="/dev/ttyS0", baudrate=9600, stopbits=2, timeout=1, **kwargs):
        print("[MOCK CONN] connecting...")

    def disconnect(self):
        print("[MOCK CONN] disconnecting...")
        print("[MOCK CONN] disconnected")

    def test_connection(self):
        return True

    def send_command(self, command):
        from time import time, sleep
        print("[" + str(time()) + "] SENT: " + command)
        sleep(1)
        return ""

    def set_speeds(self, left, right):
        if self.move_enabled:
            self.send_command("D," + str(int(left)) + "," + str(int(right)))
            now = time()
            self.command_logger.register_cmd(left, right, now)

    def go(self, speed):
        self.set_speeds(speed, speed)

    def stop(self):
        self.go(0)

    def turn(self, left, right):
        self.set_speeds(left, right)

    def move_by_count(self, left_counts, right_counts):
        print("Moving by %f, %f. Note that this is not counted by command_logger" % (left_counts, right_counts))
        self.send_command("C," + str(int(left_counts)) + "," + str(int(right_counts)))

    def set_counts(self, left_count, right_count):
        self.send_command("G," + str(left_count) + "," + str(right_count))

    def read_ir(self):
        return [0] * 8

    def read_ambient(self):
        pass

    def read_counts(self):
        return [0, 0]
