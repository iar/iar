#!/usr/bin/env python

from __future__ import unicode_literals, print_function

from time import sleep, time
import argparse

from control import with_connection


@with_connection
def test_time(conn, iterations=5):
    conn.verbose = False
    times = []
    for x in xrange(iterations):
        start = time()
        conn.read_ir()
        times.append(time() - start)
        print("Run %d: Obtained sensor results in %s" % (x, times[-1]))
    print("Average response time: %s" % (sum(times) / iterations))

@with_connection
def test_calibration(conn):
    conn.verbose = False

    from calibration import Ir2Cm, Sensor
    ir2cm = Ir2Cm()

    while True:
        ir = conn.read_ir()
        output = ""
        for direction in Sensor.DIRECTIONS:
            output += direction + ": " + str(ir2cm.ir2cm(ir, direction)) \
                + "cm, "
        print(output[:-2])
        sleep(1)


@with_connection
def main(conn):
    conn.verbose = False
    while True:
        print(conn.read_ir())
        sleep(2)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Launches sensors tests.")
    parser.add_argument(
        "--time",
        help="Measure the time it takes to send the sensor read command and "
             "read the results",
        action="store_true"
    )
    parser.add_argument(
        "--calibration",
        help="Using calibrations, estimate the distance from each sensor to "
             "its nearest object",
        action="store_true"
    )
    args = parser.parse_args()
    if args.time:
        test_time()
    elif args.calibration:
        test_calibration()
    else:
        main()
